"""
Nathan Ramsay-Vejlens
Killian Balland
11-2021
"""
import pickle
import math
import random
import time
from LRUCache import LRUCache
from training_board import *

class Minimax:

    def __init__(self, max_branching: int = 3):
        if max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")
        
        self.max_branching: int = max_branching
        
        """ 
        self._values : Scores returned by get_score for the player. 
        Key : board, number of moves explored further ahead
        Value : score for player
        """
        self._values : dict[(TrainingBoard, int), int] = dict()
        """ 
        self._best_branching_actions : Action evaluation heuristics. 
        Key : board, player, branching factor
        Value : branching evaluation, branching action, new board after action
        """
        self._best_branching_actions : dict[(TrainingBoard, Player, int), list[tuple[int, Action, TrainingBoard]]] = dict()
        """ 
        self._shortest_path : First position on shortest path and length of path. 
        Key : board, player
        Value : first position on path, length of path
        """
        self._shortest_path : LRUCache = LRUCache(300000) 
    
    def minimax(self, board: TrainingBoard, player: Player, time_left_total: float):
        """
        Minimax function with time limit.
        As long as inside time limit, it will increase max depth of 1 and redo minimax search.
        """ 
        # If no more walls to play just move along shortest path
        if board.nb_walls[player] == 0:
            pos = self.get_shortest_path(board, player)[0]
            return 0, ('P', pos[0], pos[1])

        max_time = min(time_left_total/10, 13)

        start_time = time.time()
        max_depth = 1
        value, action_returned = self.max_value(player, board, -math.inf, math.inf, max_depth)
        time_used = time.time() - start_time
        time_left = max_time - time_used

        while time_used * (self.max_branching + 0.1) < time_left: 
            start_time = time.time()
            max_depth += 1
            value, action_returned = self.max_value(player, board, -math.inf, math.inf, max_depth)
            time_used = time.time() - start_time
            time_left = time_left - time_used
        
        return value, action_returned

    def max_value(self, player: Literal[0, 1], state: TrainingBoard, alpha: float, beta: float, max_depth: int):
        if state.is_finished():
            try:
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        if max_depth <= 0:
            try:
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        max_value, best_action = -math.inf, None
        best_branching_actions = self.get_best_branching_actions(state, player)

        branching = 0
        for (_, action, new_state) in best_branching_actions:
            if best_action is None:
                best_action = action

            if (new_state, max_depth) in self._values:
                action_value = self._values[(new_state, max_depth)]
            else :
                action_value, _ = self.min_value(player, new_state, alpha, beta, max_depth - 1)
                self._values[(new_state, max_depth)] = action_value

            if action_value == max_value and action[0] == 'P' and best_action[0] == 'P': # Two moves with same value
                if abs(state.goals[player] - action[1]) < abs(state.goals[player] - best_action[1]): 
                    best_action = action # Keep move that is closer goal in straight line

            if action_value > max_value :
                best_action = action
                max_value = action_value
                alpha = max(alpha, action_value)

            if action_value >= beta:
                return max_value, best_action
            
            branching += 1
            if branching >= self.max_branching:
                break

        return max_value, best_action

    def min_value(self, player: Literal[0, 1], state: TrainingBoard, alpha: float, beta: float, max_depth: int):
        if state.is_finished():
            try:
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        if max_depth <= 0:
            try:
                return self.get_score(state, player), None 
            except Exception:
                return 0, None
            
        min_value, best_action = math.inf, None
        other_player = (player + 1) % 2
        best_branching_actions = self.get_best_branching_actions(state, other_player)

        branching = 0
        for (_, action, new_state) in best_branching_actions:
            if best_action is None:
                best_action = action
   
            if (new_state, max_depth) in self._values:
                action_value = self._values[(new_state, max_depth)]
            else :
                action_value, _ = self.max_value(player, new_state, alpha, beta, max_depth - 1)
                self._values[(new_state, max_depth)] = action_value

            if action_value < min_value:
                best_action = action
                min_value = action_value
                beta = min(beta, action_value)

            if action_value <= alpha:
                return min_value, best_action

            branching += 1
            if branching >= self.max_branching:
                break

        return min_value, best_action
    
    def get_best_branching_actions(
        self, board: TrainingBoard, player: Player, max_branching: int = None) -> list[tuple[float, Action, TrainingBoard]]:

        if max_branching is None:
            max_branching = self.max_branching
        elif max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")

        if (board, player) in self._best_branching_actions : 
            return self._best_branching_actions[(board, player)]

        best_branching_actions = []
        branching_actions = []

        for branching_action in board.get_actions(player, legal_only=False):
            try:
                action_evaluation = self.action_evaluation(
                    board, player, branching_action
                )

                new_board = board.clone().play_action(branching_action, player)
                branching_actions.append((action_evaluation, branching_action, new_board))

            except Exception:
                pass

        branching_actions.sort(key=lambda element: (-element[0], element[1][0]))

        for i in range(5):
            if i >= len(branching_actions):
                break
            best_branching_actions.append(branching_actions[i])

        self._best_branching_actions[(board, player)] = best_branching_actions

        return self._best_branching_actions[(board, player)]

    def get_score(self, board: TrainingBoard, player):
        """Return a score for this board for the given player.

        The score is the difference between the lengths of the shortest path
        of the player minus the one of its opponent. It also takes into
        account the remaining number of walls.
        """
        self.get_shortest_path(board, player)
        
        other_player = 1 - player
        self.get_shortest_path(board, other_player)

        score = self._shortest_path.get((board, other_player))[1] - \
                self._shortest_path.get((board, player))[1]
        if not score:
            score = board.nb_walls[player] - board.nb_walls[other_player]
        return score

    ### Pickling
    def save(self, filepath: str):
        try:
            with open(filepath+'_scores.pickle', "wb") as file:
                    pickle.dump(self._values, file, pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            print(e)
            print('Error saving _values!')

        try:
            with open(filepath+'_shortest_path.pickle', "wb") as file:
                pickle.dump(self._shortest_path, file, pickle.HIGHEST_PROTOCOL) 
        except Exception as e:
            print(e)
            print('Error saving _shortest_path!')

        try:
            with open(filepath+'_branching_actions.pickle', "wb") as file:
                pickle.dump(self._best_branching_actions, file, pickle.HIGHEST_PROTOCOL)   
        except Exception as e:
            print(e)
            print('Error saving _branching_actions!')   

    def load(self, filepath: str):
        try:
            with open(filepath+'_scores.pickle', "rb") as file:
                self._values = pickle.load(file)
        except Exception as e:
            print(e)
            print('Error pickling _values!')

        try:
            with open(filepath+'_shortest_path.pickle', "rb") as file:
                self._shortest_path = pickle.load(file)
        except Exception as e:
            print(e)
            print('Error pickling _shortest_path!')

        try:
            with open(filepath+'_branching_actions.pickle', "rb") as file:
                self._best_branching_actions = pickle.load(file)
        except Exception as e:
            print(e)
            print('Error pickling _branching_actions!')

    ### Heuristics 
    def action_evaluation(self, board: TrainingBoard, player: Player, action: Action):
        """
        Englobing function that returns evaluation value of action.
        We want to maximize number of moves to goal for our adversary and
        minimize number of moves to goal for us.
        
        Returns sum of variation in moves (+ for adversary, - or + for player)
        """
        difference_player = self.variation_closer_goal(board, player, action)

        difference_adv = 0
        # Moving can make adversary path shorter if he can jump over player
        # we don't take moves into account for blocking adversary
        if action[0] != 'P': 
            difference_adv = self.block_closer_goal(board, player, action)

        return difference_adv + difference_player

    def variation_closer_goal(self, board: TrainingBoard, player: Player, action: Action):
        """
        We want to minimize number of moves left to get to goal for our player.
        Returns variation in number of moves (can be negative if increases number of moves). 
        Between -x and 1 including 0 (status quo).
        """
        difference_player = 0
        self.get_shortest_path(board, player)

        new_board = board.clone().play_action(action, player)
        if new_board.is_finished(): # Higher value to priorize winning
            return 2

        self.get_shortest_path(new_board, player)

        if action[0] != 'P': #if wall, check if blocks player
            new_value = self._shortest_path.get((new_board, player))[1]
            difference_player = self._shortest_path.get((board, player))[1] - new_value # -x to 0
            # Placing wall can make path shorter because of jump over adversary
            if difference_player > 0: # Usually adversary won't stay in that position and jump is too late in path
                difference_player = 0   # so we don't consider these cases
        elif (action[1], action[2]) == self._shortest_path.get((board, player))[0]: #if move on shortest path
            difference_player = 1
            
        return difference_player
        
    def block_closer_goal(self, board: TrainingBoard, player: Player, action: Action):
        """
        We want to maximize number of moves before adversary gets to goal.
        Returns variation in number of moves to get to goal (always positive). 
        Between 0 and x.
        """
        other_player = 1 - player

        self.get_shortest_path(board, other_player)
        initial_value = self._shortest_path.get((board, other_player))[1]

        new_board = board.clone().play_action(action, player)

        self.get_shortest_path(new_board, other_player)
        new_value = self._shortest_path.get((new_board, other_player))[1]

        return new_value - initial_value

    def get_shortest_path(self, board: TrainingBoard, player: Player):
        """
        Gets shortest path to goal for player and keeps value in dict that will be pickled to be reused.
        """
        if (board, player) not in self._shortest_path.cache:
            try:  
                if board.goals[player] == board.pawns[player][0]:
                    self._shortest_path.put((board, player),([], 0))
                    return
                path = board.get_solution_path(player)
                self._shortest_path.put((board, player),(path[0], len(path)))
            except NoPath:         
                self._shortest_path.put((board, player), ([],0))

        return self._shortest_path.get((board, player))

    ### Learning
    def random_branch(self, board: TrainingBoard, player: Literal[0, 1]):
        """
        Function used for running quick matches and learn values to pickle.
        """
        actions = self.get_best_branching_actions(board, player, 5)
        return random.choice(actions)[1]
