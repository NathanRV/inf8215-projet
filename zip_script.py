# importing required modules
from zipfile import ZipFile

file_paths = ['agent_1989944_2159054.py',
                'LRUCache.py',
                'minimax.py',
                'minmax_branching_actions.pickle',
                'minmax_scores.pickle',
                'minmax_shortest_path.pickle',
                'README.md',
                'requirements.txt',
                'training_board.py']

# printing the list of all files to be zipped
print('Following files will be zipped:')
for file_name in file_paths:
    print(file_name)

# writing files to a zipfile
with ZipFile('1989944_2159054.zip','w') as zip:
    # writing each file one by one
    for file in file_paths:
        zip.write(file)

print('All files zipped successfully!')  