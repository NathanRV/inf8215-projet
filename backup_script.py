import shutil

def backup(backup_num: int):
    print('Backup number ', backup_num)
    src_path = r".\minmax_branching_actions.pickle"
    dst_path = r".\pickle_backup\minmax"+str(backup_num)+"_branching_actions.pickle"
    shutil.copy(src_path, dst_path)
    #print('Copied branching actions')

    src_path = r".\minmax_scores.pickle"
    dst_path = r".\pickle_backup\minmax"+str(backup_num)+"_scores.pickle"
    shutil.copy(src_path, dst_path)
    #print('Copied scores')

    src_path = r".\minmax_shortest_path.pickle"
    dst_path = r".\pickle_backup\minmax"+str(backup_num)+"_shortest_path.pickle"
    shutil.copy(src_path, dst_path)
    #print('Copied shortest path')