from training_board import TrainingBoard
import pickle
from typing import Literal
import os
import shutil

shortest_path : dict[(TrainingBoard, Literal[0,1]), list] = dict()
shortest_path_modified : dict[(TrainingBoard, Literal[0,1]), ((int,int), int)] = dict()

with open('minmax_shortest_path.pickle', "rb") as file:
    shortest_path = pickle.load(file)

print('Finished loading file')

for key in shortest_path.keys():
    if len(shortest_path[key]) != 0:
        shortest_path_modified[key] = (shortest_path[key][0], len(shortest_path[key]))
    else : 
        shortest_path_modified[key] = ([], len(shortest_path[key]))

print('Finished modifying dict')

with open('minmax_shortest_path_modif.pickle', "wb") as file:
    pickle.dump(shortest_path_modified, file, pickle.HIGHEST_PROTOCOL)

print('Finished saving file')
    