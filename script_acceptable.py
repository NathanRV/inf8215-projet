
from quoridor import *
import math
import pickle
from heuristics import Heuristics
from typing import Literal

acceptable_actions : dict[(str, Literal[0,1], (str, int, int)), bool] = dict()

converted :  dict[(str, Literal[0,1], (str, int, int), int, int), bool] = dict()


with open('minmax_acceptable_actions.pickle', "rb") as file:
    acceptable_actions = pickle.load(file)


for key in acceptable_actions.keys():
    new_key = (key[0], key[1], key[2], 1, 2)
    converted[new_key] = acceptable_actions[key]


with open('minmax_acceptable_actions.pickle', "wb") as file:
    pickle.dump(converted, file, pickle.HIGHEST_PROTOCOL)

    