""" 
Heuristic functions to evaluate states. 
Some are inspired by the ones mentioned in "A Quoridor-playing Agent" by P.J.C. Mertens on June 21, 2006.
"""

import pickle
from typing import Literal
from quoridor import Board
from training_board import Action, Player, TrainingBoard
Player = Literal[0,1]
Action = tuple[str, int, int]



class Heuristics():
    def __init__(self):
        """
        """
        self._known_values: dict[tuple[BoardRepresentation, Player], int] = dict()
        self.pos_feat_factor = 1
        self.pos_diff_factor = 1
        self.n_line_player_factor = 1
        self.n_line_adversary_factor = 1
        self._shortest_path : dict[(TrainingBoard, Literal[0,1]), list] = dict()
        
    def load(self, filepath: str):
        with open(filepath+'_shortest_path.pickle', "rb") as file:
            self._shortest_path = pickle.load(file)

    """SCORE EVALUATION SECTION, WE ARE LOOKING TO MAXIMIZE THESE VALUES.
    USED IN MINMAX FUNCTIONS"""
      
    def action_evaluation(self, board: Board, player: Player, action: Action, n = 8):
        """
        Englobing function that returns evaluation value of action.
        """
        #print('Evaluating action : ', action)
        #if n == board.size - 1:
            # Shortest path to goal
        evaluation = self.block_comparison_closer_goal(board, player, action)
        #else :
            # Can be used to get shortest path to certain number of lines
        #    evaluation = self.block_comparison_n_line_closer_goal(board, player, action, n)
        #print('Action ', action, ' for depth n : ', n, 'has value', evaluation)

        return evaluation

    def score_approximation(self, board: Board, player: Player):
        """ 
        TODO : should add some defense heuristic moves_to_goal_adversary or moves_next_line_adversary
        but might just be the same as get_score
        
        Heuristic used instead of get_score function
        Returns position feature + position difference feature - moves needed to get to next line
        """
        pos_feat = self.position_feature(board, player)
        pos_diff = self.position_difference(board, player)
        moves_next_line = self.n_line_closer_goal(board, player, 1)
        moves_to_goal = self.n_line_closer_goal(board, player, 9)
        print('Position feature player :', pos_feat)
        print('Position feature difference :', pos_diff)
        print('Moves to next line :', moves_next_line)
        print('Moves to goal :', moves_to_goal)
        #total = pos_feat + pos_diff - moves_next_line
        total = pos_feat + pos_diff - moves_to_goal
        print('Total score approx is ', total)

        return total
     
    # Feature 1 : position feature, straight distance between start line and player
    def position_feature(self, board: Board, player: Player):
        """
        Returns the distance in straight line between player's pawn and their adversary's goal.
        """
        return abs(board.pawns[player][0] - board.goals[(player + 1) % 2])

    #Feature 2 from paper
    def position_difference(self, board: Board, player: Player):
        """
        Returns the difference in position feature values between player and adversary.
        Returns 0 if adversary has the advantage.
        """
        difference = self.position_feature(board, player) - \
            self.position_feature(board, (player + 1) % 2)
        if difference > 0:
            return difference
        return 0

    # On player, can return -x,...,-1,0,1 
    def variation_closer_goal(self, board: TrainingBoard, player: Player, action: Action):
        """
        We want to minimize number of moves before goal if our player.
        Returns variation in number of moves (can be negative if increases number of moves).
        """
        difference_player = 0
        if (board, player) not in self._shortest_path:
            self._shortest_path[(board, player)] = board.get_solution_path(player)
            #self._shortest_path[(board, player)] = self.get_path_n_line_closer_goal(board, player, 8)

        new_board = board.clone().play_action(action, player)
        if (new_board, player) not in self._shortest_path:
            self._shortest_path[(new_board, player)] = new_board.get_solution_path(player)
            #self._shortest_path[(new_board, player)] = self.get_path_n_line_closer_goal(new_board, player, 8)

        if action[0] != 'P': #if wall, check if blocks player
            new_value = len(self._shortest_path[(new_board, player)])
            difference_player = len(self._shortest_path[(board, player)]) - new_value #-x
            # Placing wall can make path shorter because of jump over adversary
            if difference_player > 0: # Usually adversary won't stay in that position and jump is too late in path
                difference_player = 0
        elif (action[1], action[2]) == self._shortest_path[(board, player)][0]: #if move on shortest path
            difference_player = 1
            
        return difference_player
        
    # On adversary, wall can increase more than one 0,1,...,x
    def block_closer_goal(self, board: TrainingBoard, player: Player, action: Action):
        """
        We want to maximize number of moves before closer to goal for our adversary.
        Returns variation in number of moves (always positive).
        """
        other_player = 1 - player

        if (board, other_player) not in self._shortest_path:
            self._shortest_path[(board, other_player)] = board.get_solution_path(other_player)
            #self._shortest_path[(board, other_player)] = self.get_path_n_line_closer_goal(board, other_player, 8)
        initial_value = len(self._shortest_path[(board, other_player)])

        new_board = board.clone().play_action(action, player)

        if (new_board, other_player) not in self._shortest_path:
            self._shortest_path[(new_board, other_player)] = new_board.get_solution_path(other_player)
            #self._shortest_path[(new_board, other_player)] = self.get_path_n_line_closer_goal(new_board, other_player, 8)
        new_value = len(self._shortest_path[(new_board, other_player)])

        return new_value - initial_value

    # Final heuristic used in action evaluation
    def block_comparison_closer_goal(self, board: TrainingBoard, player: Player, action: Action):
        """
        We want to maximize number of moves before n line closer for our adversary and
        minimize number of moves before n line closer for us.
        
        Returns sum of variation in moves (+ for adversary, - for player)
        """
        difference_player = self.variation_closer_goal(board, player, action)

        difference_adv = 0
        # Moving can make adversary path shorter if he can jump over player
        # we don't take this into account
        if action[0] != 'P': 
            difference_adv = self.block_closer_goal(board, player, action)

        return difference_adv + difference_player


    # On player, can return -x,...,-1,0,1 
    def variation_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to minimize number of moves before n line closer if our player.
        Returns variation in number of moves (can be negative if increases number of moves).
        """
        difference_player = 0
        initial_path = self.get_path_n_line_closer_goal(board, player, n)

        new_board = board.clone().play_action(action, player)

        if (action[1], action[2]) == initial_path[0]: #if move on shortest path
            difference_player = 1
        elif action[0] != 'P': #if wall, check if blocks player
            new_value = self.n_line_closer_goal(new_board, player, n)
            difference_player = len(initial_path) - new_value #-x

        return difference_player

    # On adversary, wall can increase more than one 0,1,...,x
    def block_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to maximize number of moves before n line closer for our adversary.
        Returns variation in number of moves (always positive).
        """
        initial_value = self.n_line_closer_goal(board, 1-player, n)
        new_board = board.clone().play_action(action, player)
        new_value = self.n_line_closer_goal(new_board, 1-player, n)
        return new_value - initial_value

    # Final heuristic used in action evaluation
    def block_comparison_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to maximize number of moves before n line closer for our adversary and
        minimize number of moves before n line closer for us.
        
        Returns sum of variation in moves (+ for adversary, - for player)
        """
        difference_player = self.variation_n_line_closer_goal(board, player, action, n)
        difference_adv = self.block_n_line_closer_goal(board, player, action, n)

        #print('Difference player :', difference_player)
        #print('Difference adversary :', difference_adv)

        return difference_adv + difference_player


    """HEURISTICS SECTION, WE ARE LOOKING TO MINIMIZE THESE VALUES. 
    CAN BE USED IN MINMAX FUNCTIONS IF USING A FACTOR AND DIVIDING BY VALUE RETURNED
    BUT MOSTLY REUSED IN SCORE EVALUATION SECTION"""

    def get_path_n_line_closer_goal(self, board: Board, player: Player, n = 1):
        """ Slightly modified function from quoridor.py get_shortest_path()
        Returns a shortest path for player to reach n lines closer to their goal
        if player is on its goal, the shortest path is an empty list
        if no path exists, exception is thrown.
        """

        def get_pawn_moves(pos):
            (x, y) = pos
            positions = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1), (x - 1, y + 1),
                (x + 2, y), (x - 2, y), (x, y + 2), (x, y - 2)]
            moves = []
            for new_pos in positions:
                if board.is_pawn_move_ok(pos, new_pos,
                    board.pawns[(player + 1) % 2]):
                    moves.append(new_pos)
            return moves

        visited = [[False for i in range(board.size)] for i in range(board.size)]
        # Predecessor matrix in the BFS
        prede = [[None for i in range(board.size)] for i in range(board.size)]

        neighbors = [board.pawns[player]]
        current_x = neighbors[0][0]
        goal = board.goals[player]

        difference = goal - current_x
        if difference >= 0 : # going down
            if difference < n: # less than n lines to go
                next_x = goal
            else :
                next_x = current_x + n
        else : # going up
            if abs(difference) < n: # less than n lines to go
                next_x = goal
            else:
                next_x = current_x - n

        while len(neighbors) > 0:
            neighbor = neighbors.pop(0)
            (x, y) = neighbor
            visited[x][y] = True
            if x == next_x:
                succ = [neighbor]
                curr = prede[x][y]
                while curr is not None and curr != board.pawns[player]:
                    succ.append(curr)
                    (x_, y_) = curr
                    curr = prede[x_][y_]
                succ.reverse()
                return succ
            unvisited_succ = [(x_, y_) for (x_, y_) in
                get_pawn_moves(neighbor) if not visited[x_][y_]]
            for n_ in unvisited_succ:
                (x_, y_) = n_
                if not n_ in neighbors:
                    neighbors.append(n_)
                    prede[x_][y_] = neighbor
        return []

    # Feature 3 and feature 4 from Mertens's paper depending on player chosen if n =1
    def n_line_closer_goal(self, board: Board, player: Player, n = 1):
        """
        Returns number of moves to reach n lines closer to their goal
        if player is on its goal, returns 0
        """
        if board.pawns[player][0] == board.goals[player]:
            return 0
        return len(self.get_path_n_line_closer_goal(board, player, n))

