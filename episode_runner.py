import random

import numpy as np
from agent_1989944_2159054 import *
import math
import os


def greedy_play(board: TrainingBoard, player: Player) -> Action:
    if random.random() <= 0.25:
        oppo_y, oppo_x = board.pawns[1-player]
        oppo_goal_y = board.goals[1-player]
        wall_actions = board.get_wall_moves(player)

        candidate_walls = []
        if oppo_goal_y < oppo_y:
            for wall_action in wall_actions:
                wall_dir, wall_y, wall_x = wall_action
                if wall_dir == 'WH' and wall_y == oppo_y - 1 and wall_x in (oppo_x, oppo_x - 1):
                    candidate_walls.append(wall_action)
        else:
            for wall_action in wall_actions:
                wall_dir, wall_y, wall_x = wall_action
                if wall_dir == 'WH' and wall_y == oppo_y and wall_x in (oppo_x, oppo_x - 1):
                    candidate_walls.append(wall_action)

        if len(candidate_walls) > 0:
            choice = random.choice(candidate_walls)
            return choice

    shortest_path = board.get_solution_path(player)

    next_y, next_x = shortest_path[0]
    next_move = ('P', next_y, next_x)
    return next_move

def random_play(board: TrainingBoard, player: Player) -> Action:
    actions = list(board.get_actions(player))
    return random.choice(actions)

def execute_episode(minimax: Minimax, num_iteration: str) -> list:
    board: TrainingBoard = TrainingBoard()
    current_player: int = 0
    trained_player: int = random.randint(0, 1)
    execution_results = []

    #print('Iteration #'+num_iteration+' PARENT PROCESS ID :',os.getppid()+'\n Iteration #'+num_iteration+' PROCESS ID :', os.getpid()+'\n')
    minimax.load('minmax'+num_iteration)

    while not board.is_finished():
        if current_player == trained_player:
            value, action = minimax.max_value(current_player, board, -math.inf, math.inf, 0)
            #print('Action chosen is ', action, 'with value of ', value)
        else:
            #action = greedy_play(board, current_player)

            action = minimax.random_branch(board, current_player)
            #print('Random branch action chosen is ', action)

        board.play_action(action, current_player)
        current_player = (current_player + 1) % 2

    minimax.save('minmax'+num_iteration)
    #print('Finished game, board is \n', board)

    final_score = board.get_result(trained_player)
    return final_score

