# Installation des dépendances

Lancez simplement la commande suivante :
```console
pip install -r requirements.txt
```

# Fichiers nécessaires au fonctionnement de l'agent

Tous les fichiers nécessaire au fonctionnement de l'agent se trouvent à la racine et sont les suivants :

- `agent_1989944_2159054.py`
- `LRUCache.py`
- `minimax.py`
- `minmax_branching_actions.pickle`
- `minmax_scores.pickle`
- `minmax_shortest_path.pickle`
- `requirements.txt`
- `training_board.py`