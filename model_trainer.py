import logging
import argparse
import tqdm
#import coloredlogs
import multiprocessing as mp
import update_script as updater
import backup_script as backup
import sys

from episode_runner import execute_episode
from agent_1989944_2159054 import *

log = logging.getLogger(__name__)
#coloredlogs.install(level='INFO')


class ModelTrainer:
    def __init__(
        self,
        #learning_model: LearningModel,
        args: argparse.Namespace = argparse.Namespace(),
    ):
        #self.learning_model = learning_model
        self.args = args

    def batching(self, examples: list, batch_size: int):
        for i in range(0, len(examples), batch_size):
            yield examples[i : i + batch_size]

    def train_model(self):
        log.info("Initializing model training...")
        minimax = Minimax(
            max_branching = 3)
        
        updater.copy_pickles(self.args.num_iterations)

        with mp.Pool(
            processes=min(mp.cpu_count() - 1, self.args.num_iterations)
        ) as pool:

            with tqdm.tqdm(
                total=self.args.num_iterations,
                desc="Runned iterations",
                unit="iteration",
            ) as progress_bar:
                executions_results = []

                def collect_results(execution_results: int):
                    progress_bar.update(1)
                    executions_results.append(execution_results)

                for num_iteration in range(1, self.args.num_iterations + 1):
                    log.info("----ITERATION %d/%d----", num_iteration, self.args.num_iterations)
                    pool.apply_async(
                        execute_episode,
                        args=(minimax,str(num_iteration)),
                        callback=collect_results,
                    )

                pool.close()
                pool.join()

                log.info("Iterations successfully executed.")
        
        updater.update_pickles(self.args.num_iterations)
        return 0

def main(args):
    log.info("Parsing arguments...")

    def special_int_arg(arg_name: str):
        def special_arg_parser(s: str) -> int:
            value = int(s)
            if value < 1:
                raise argparse.ArgumentTypeError("%s is not a valid %s" % (s, arg_name))
            return value

        return special_arg_parser

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-it",
        "--num-iterations",
        dest="num_iterations",
        type=special_int_arg("num iterations"),
        default=20,
        help="set the number of iterations for the update of the model's parameters (default: 20)",
        metavar="NUM_ITERATIONS",
    )
    

    parser.add_argument(
        "-ne",
        "--num-episodes",
        dest="num_episodes",
        type=special_int_arg("num episodes"),
        default=50,
        help="set the number of episodes per iteration (default: 50)",
        metavar="NUM_EPISODES",
    )

    parser.add_argument(
        "-lf", "--load-file", dest="load_file", help="file to initially load", metavar="LOAD_FILE"
    )

    args = parser.parse_args(args)

    log.info("Loading trainer...")
    trainer = ModelTrainer(args)

    log.info("Starting training model...")

    trainer = ModelTrainer(args)

    breakpoint = 5
    backup_num = 0

    #for i in range(10):
        #print('For loop ', i)
        #if i % breakpoint == 0:
        #    backup.backup(backup_num)
        #    backup_num += 1
    trainer.train_model()



if __name__ == "__main__":
    main(sys.argv[1:])