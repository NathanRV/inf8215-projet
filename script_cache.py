from training_board import TrainingBoard
import pickle
from typing import Literal
import os
import shutil
from agent_1989944_2159054 import *

shortest_path : LRUCache = LRUCache(350000)
shortest_path_combined : LRUCache = LRUCache(300000)



try:
    with open('minmax_shortest_path.pickle', "rb") as file:
        shortest_path = pickle.load(file)
    for k in shortest_path.cache.keys():
        shortest_path_combined.put(k, shortest_path.cache[k])
except Exception as e:
    print(e)
    print('Error pickling/updating path!', i)
    pass

try:
    with open('minmax_shortest_path.pickle', "wb") as file:
        pickle.dump(shortest_path_combined, file, pickle.HIGHEST_PROTOCOL)
except Exception:
    print('Error saving !')
pass