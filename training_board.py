"""
Common definitions for the Arlecchino players.
Author: Cyrille Dejemeppe <cyrille.dejemeppe@uclouvain.be>
Copyright (C) 2013, Université catholique de Louvain

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

"""

from quoridor import InvalidAction, NoPath
from typing import Literal
import collections
import marshal
import numpy as np

PLAYER1 = 0
PLAYER2 = 1

Player = Literal[0, 1]
Action = tuple[str, int, int]
Position = tuple[int, int]


class TrainingBoard:

    """
    Representation of a Quoridor Board for training.

    """

    def __init__(self):
        """
        Constructor of the representation for a quoridor game of size 9.
        The representation can be initialized by a percepts
        If percepts==None:
            player 0 is position (4,0) and its goal is to reach the row 8
            player 1 is position (4,8) and its goal is to reach the row 0
            each player owns 10 walls and there is initially no wall on the
            board
        """

        self.size: int = 9
        self.rows: int = self.size
        self.cols: int = self.size
        self.starting_walls: int = 10
        self.pawns: list[Position] = [(0, 4), (8, 4)]
        self.goals: list[int] = [8, 0]
        self.nb_walls: list[int] = [self.starting_walls, self.starting_walls]
        self.horiz_walls: list[Position] = []
        self.verti_walls: list[Position] = []

    def clone(self):
        """Return a clone of this object."""
        clone_board = TrainingBoard()
        clone_board.pawns[0] = self.pawns[0]
        clone_board.pawns[1] = self.pawns[1]
        clone_board.goals[0] = self.goals[0]
        clone_board.goals[1] = self.goals[1]
        clone_board.nb_walls[0] = self.nb_walls[0]
        clone_board.nb_walls[1] = self.nb_walls[1]
        for (x, y) in self.horiz_walls:
            clone_board.horiz_walls.append((x, y))
        for (x, y) in self.verti_walls:
            clone_board.verti_walls.append((x, y))
        return clone_board

    def is_simplified_pawn_move_ok(self, former_pos: Position, new_pos: Position) -> bool:
        """Returns True if moving one pawn from former_pos to new_pos
        is valid i.e. it respects the rules of quoridor (without the
        heap move above the opponent)
        """

        (row_form, col_form) = former_pos
        (row_new, col_new) = new_pos

        if (row_form == row_new and col_form == col_new) or \
                row_new >= self.size or row_new < 0 or \
                col_new >= self.size or col_new < 0:
            return False

        wall_right = ((row_form, col_form) in self.verti_walls) or \
            ((row_form - 1, col_form) in self.verti_walls)

        wall_left = ((row_form - 1, col_form - 1) in self.verti_walls) or \
                    ((row_form, col_form - 1) in self.verti_walls)

        wall_up = ((row_form - 1, col_form - 1) in self.horiz_walls) or \
            ((row_form - 1, col_form) in self.horiz_walls)

        wall_down = ((row_form, col_form) in self.horiz_walls) or \
                    ((row_form, col_form - 1) in self.horiz_walls)

        if row_new == row_form + 1 and col_new == col_form:
            return not wall_down

        if row_new == row_form - 1 and col_new == col_form:
            return not wall_up

        if row_new == row_form and col_new == col_form + 1:
            return not wall_right

        if row_new == row_form and col_new == col_form - 1:
            return not wall_left

        return False

    def is_pawn_move_ok(self, former_pos: Position, new_pos: Position, opponent_pos: Position) -> bool:
        """Returns True if moving one pawn from former_pos to new_pos is
        valid i.e. it respects the rules of quoridor
        """

        (x_form, y_form) = former_pos
        (x_new, y_new) = new_pos
        (x_op, y_op) = opponent_pos

        if (x_op == x_new and y_op == y_new) or (x_form == x_new and y_form == y_new):
            return False

        if np.abs(x_form - x_op) + np.abs(y_form - y_op) + np.abs(x_new - x_op) + np.abs(y_new - y_op) == 2:
            if not self.is_pawn_move_ok(opponent_pos, new_pos, (-10, -10)) or not self.is_pawn_move_ok(former_pos, opponent_pos, (-10, -10)):
                return False

            if (x_form - x_new) ** 2 + (y_form - y_new) ** 2 == 2:
                return not self.is_pawn_move_ok(opponent_pos,
                                                (x_op + (x_op - x_form),
                                                 y_op + (y_op - y_form)),
                                                (-10, -10))

            return True

        return self.is_simplified_pawn_move_ok(former_pos, new_pos)

    def paths_exist_wall(self, pos: Position, is_horiz: bool) -> bool:
        """Returns True if there exists a path from both players to
        at least one of their respective goals; False otherwise.
        """

        if is_horiz:
            self.horiz_walls.append(pos)
            try:
                self.get_solution_path(PLAYER1, shortest_path=False)
                self.get_solution_path(PLAYER2, shortest_path=False)
            except NoPath:
                self.horiz_walls.pop()
                return False

            self.horiz_walls.pop()
        else:
            self.verti_walls.append(pos)
            try:
                self.get_solution_path(PLAYER1, shortest_path=False)
                self.get_solution_path(PLAYER2, shortest_path=False)
            except NoPath:
                self.verti_walls.pop()
                return False

            self.verti_walls.pop()

        return True
    
    def get_available_pawn_moves(self, pos: Position, player: Player, visited: np.array, shortest_path: bool = True) -> list[Position]:
        (x, y) = pos

        if shortest_path:
            positions = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                            (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1),
                            (x - 1, y + 1), (x + 2, y), (x - 2, y), (x, y + 2),
                            (x, y - 2)]
        else:
            positions = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                            (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1), (x - 1, y + 1)]

        moves = []

        for new_pos in positions:
            (x_, y_) = new_pos
            if x_ < self.size and x_ >= 0 and y_ < self.size and y_ >= 0:
                if not visited[x_, y_] and self.is_pawn_move_ok(pos, new_pos, self.pawns[(player + 1) % 2]):
                    moves.append(new_pos)

        return moves

    def get_solution_path(self, player: Player, shortest_path: bool = True) -> list[Position]:
        """ Returns a shortest path for player to reach its goal
        if player is on its goal, the shortest path is an empty list
        if no path exists, exception is thrown.
        """

        start_pos = self.pawns[player]
        (start_x, start_y) = start_pos
        goal = self.goals[player]

        if start_x == goal:
            return []


        visited: np.array[bool] = np.full((self.size, self.size), False)

        prede: np.array[Position] = np.full((self.size, self.size), None)

        neighbors = collections.deque([start_pos])
        visited[start_x][start_y] = True

        while len(neighbors) > 0:
            neighbor = neighbors.popleft()
            (x, y) = neighbor

            if x == goal:
                succ = [neighbor]
                curr = prede[x][y]

                while curr is not None and curr != start_pos:
                    succ.append(curr)
                    (x_, y_) = curr
                    curr = prede[x_][y_]

                succ.reverse()

                return succ

            for successor in self.get_available_pawn_moves(neighbor, player, visited, shortest_path):
                (x_, y_) = successor
                visited[x_][y_] = True
                neighbors.append(successor)
                prede[x_][y_] = neighbor

        raise NoPath()

    def min_steps_before_victory(self, player: Player) -> int:
        """Returns the minimum number of pawn moves necessary for the
        player to reach its goal raw.
        """

        return len(self.get_solution_path(player))

    def add_wall(self, pos, is_horiz, player):
        """Player adds a wall in position pos. The wall is horizontal
        if is_horiz and is vertical otherwise.
        if it is not possible to add such a wall because the rules of
        quoridor game don't accept it nothing is done.
        """

        if self.nb_walls[player] > 0:
            if is_horiz:
                self.horiz_walls.append(pos)
            else:
                self.verti_walls.append(pos)

            self.nb_walls[player] -= 1

    def move_pawn(self, new_pos: Position, player: Player):
        """Modifies the state of the board to take into account the
        new position of the pawn of player.
        """

        self.pawns[player] = new_pos

    def is_wall_possible_here(self, pos: Position, is_horiz: bool, path_exists: bool = True) -> bool:
        """
        Returns True if it is possible to put a wall in position pos
        with direction specified by is_horiz.
        """

        (x, y) = pos

        if x >= self.size - 1 or x < 0 or y >= self.size - 1 or y < 0:
            return False

        if pos in self.horiz_walls or pos in self.verti_walls:
            return False

        wall_horiz_right = (x, y + 1) in self.horiz_walls
        wall_horiz_left = (x, y - 1) in self.horiz_walls
        wall_vert_up = (x - 1, y) in self.verti_walls
        wall_vert_down = (x + 1, y) in self.verti_walls

        if is_horiz and (wall_horiz_right or wall_horiz_left):
            return False

        elif wall_vert_up or wall_vert_down:
            return False

        if path_exists and not self.paths_exist_wall(pos, is_horiz):
            return False

        return True

    def get_pawn_moves(self, player: Player) -> list[Action]:
        """Returns legal moves for the pawn of player."""

        (x, y) = self.pawns[player]

        positions = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                     (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1), (x - 1, y + 1),
                     (x + 2, y), (x - 2, y), (x, y + 2), (x, y - 2)]

        moves = []

        for new_pos in positions:
            if self.is_pawn_move_ok(self.pawns[player], new_pos, self.pawns[(player + 1) % 2]):
                moves.append(('P', new_pos[0], new_pos[1]))

        return moves

    def get_wall_moves(self, player: Player, legal_only: bool = True) -> list[Action]:
        """Returns legal wall placements (adding a wall
        somewhere) for player.
        """

        positions = []
        moves = []

        if self.nb_walls[player] <= 0:
            return moves

        for i in range(self.size - 1):
            for j in range(self.size - 1):
                positions.append((i, j))

        for pos in positions:
            if self.is_wall_possible_here(tuple(pos), True, legal_only):
                moves.append(('WH', pos[0], pos[1]))

            if self.is_wall_possible_here(tuple(pos), False, legal_only):
                moves.append(('WV', pos[0], pos[1]))

        return moves

    def get_actions(self, player: Player, legal_only: bool = True) -> list[Action]:
        """ Returns all the possible actions for player."""

        pawn_moves = self.get_pawn_moves(player)
        wall_moves = self.get_wall_moves(player, legal_only)
        pawn_moves.extend(wall_moves)

        return pawn_moves

    def is_action_valid(self, action, player):
        """Returns True if the action played by player
        is valid; False otherwise.
        """

        kind, i, j = action
        if kind == 'P':
            return self.is_pawn_move_ok(self.pawns[player], (i, j), self.pawns[(player + 1) % 2])
        elif kind == 'WH':
            wall_pos = self.is_wall_possible_here((i, j), True)
            return wall_pos
        elif kind == 'WV':
            wall_pos = self.is_wall_possible_here((i, j), False)
            return wall_pos

        return False

    def play_action(self, action: Action, player: Player, check_validity: bool = True):
        """Play an action if it is valid.

        If the action is invalid, raise an InvalidAction exception.
        Return self.

        Arguments:
        action -- the action to be played
        player -- the player who is playing

        """

        try:
            if len(action) != 3:
                raise InvalidAction(action, player)

            if check_validity and not self.is_action_valid(action, player):
                raise InvalidAction(action, player)

            kind, x, y = action

            if kind == 'WH':
                self.add_wall((x, y), True, player)
            elif kind == 'WV':
                self.add_wall((x, y), False, player)
            elif kind == 'P':
                self.move_pawn((x, y), player)
            else:
                raise InvalidAction(action, player)

        except Exception:
            raise InvalidAction(action, player)

        return self

    def is_finished(self):
        """Return whether no more moves can be made (i.e.,
        game finished).
        """

        return self.pawns[PLAYER1][0] == self.goals[PLAYER1] or self.pawns[PLAYER2][0] == self.goals[PLAYER2]

    def get_result(self, player: Player = PLAYER1):
        if not self.is_finished():
            raise Exception("Game is not yet finished!")

        if self.pawns[player][0] == self.goals[player]:
            return 1
        else:
            return -1

    def __key(self):
        return (
            marshal.dumps(sorted(self.pawns), -1),
            marshal.dumps(sorted(self.horiz_walls), -1),
            marshal.dumps(sorted(self.verti_walls), -1),
            marshal.dumps(sorted(self.nb_walls), -1),
        )

    def __hash__(self) -> int:
        return hash(self.__key())
    
    def __eq__(self, other):
        if isinstance(other, TrainingBoard):
            return self.__key() == other.__key()
        
        return False

def dict_to_training_board(dictio: dict):
    clone_board = TrainingBoard()
    clone_board.pawns[0] = tuple(dictio['pawns'][0])
    clone_board.goals[0] = dictio['goals'][0]
    clone_board.pawns[1] = tuple(dictio['pawns'][1])
    clone_board.goals[1] = dictio['goals'][1]
    for (x, y) in dictio['horiz_walls']:
        clone_board.horiz_walls.append((x, y))
    for (x, y) in dictio['verti_walls']:
        clone_board.verti_walls.append((x, y))
    clone_board.nb_walls[0] = dictio['nb_walls'][0]
    clone_board.nb_walls[1] = dictio['nb_walls'][1]
    return clone_board
