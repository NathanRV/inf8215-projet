import math
from autograd.tracer import new_box
import numpy as np
from training_board import Action, Player, TrainingBoard
import heuristic


class MinimaxLearning:
    def __init__(
        self,
        max_depth: int = 3,
        max_branching: int = 3,
        learning_model: LearningModel = LearningModel(),
    ):
        if max_depth < 1:
            raise ValueError("Max depth must be greater or equals to 1!")

        if max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")

        self.max_depth: int = max_depth
        self.max_branching: int = max_branching
        self.learning_model: LearningModel = learning_model
        self._visited_states: set[tuple[TrainingBoard, Player]] = set()

    def get_best_branching_actions(
        self, board: TrainingBoard, player: Player, max_branching: int = None
    ) -> list[tuple[float, Action, TrainingBoard]]:

        if max_branching is None:
            max_branching = self.max_branching
        elif max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")

        branching_actions = []
        branching_evaluations = []

        for branching_action in board.get_actions(player, legal_only=False):
            action_evaluation = self.learning_model.evaluation(
                board, player, branching_action
            )
            branching_actions.append(branching_action)
            branching_evaluations.append(action_evaluation)

        best_branching_actions = []

        nb_added_actions = 0
        i = len(branching_evaluations) - 1
        sorted_eval_indexes = np.argsort(branching_evaluations)

        while i >= 0 and nb_added_actions <= max_branching:
            branching_action = branching_actions[sorted_eval_indexes[i]]
            branching_evaluation = branching_evaluations[sorted_eval_indexes[i]]

            try:
                new_board = board.clone().play_action(branching_action, player)

                if (new_board, player) not in self._visited_states:
                    best_branching_actions.append(
                        (branching_evaluation, branching_action, new_board)
                    )
                    nb_added_actions += 1

            except Exception:
                pass

            i -= 1

        return best_branching_actions

    def minimax(self, board: TrainingBoard, player: Player) -> tuple[float, Action]:
        self._visited_states.add((board, player))
        return self._max_value(board, player, -math.inf, math.inf, 1)

    def _max_value(
        self,
        board: TrainingBoard,
        player: Player,
        alpha: float,
        beta: float,
        depth: int,
    ) -> tuple[float, Action]:
        if board.is_finished():
            return board.get_result(player), None

        if depth >= self.max_depth:
            return self.get_best_branching_actions(board, player, 1)[0][:2]

        other_player = (player + 1) % 2
        max_value, best_action = -math.inf, None
        best_branching_actions = self.get_best_branching_actions(board, player)

        for (_, action, new_board) in best_branching_actions:
            action_value, _ = self._min_value(
                new_board, other_player, alpha, beta, depth + 1
            )

            if action_value > max_value:
                best_action = action
                max_value = action_value
                alpha = max(alpha, action_value)

                if alpha >= beta:
                    return max_value, best_action

        return max_value, best_action

    def _min_value(
        self,
        board: TrainingBoard,
        player: Player,
        alpha: float,
        beta: float,
        depth: int,
    ) -> tuple[float, Action]:
        if board.is_finished():
            return -board.get_result(player), None

        if depth >= self.max_depth:
            value, action = self.get_best_branching_actions(board, player, 1)[0][:2]
            return -value, action

        other_player = (player + 1) % 2
        min_value, best_action = math.inf, None

        best_branching_actions = self.get_best_branching_actions(board, player)

        for (_, action, new_board) in best_branching_actions:
            action_value, _ = self._max_value(
                new_board, other_player, alpha, beta, depth + 1
            )

            if action_value < min_value:
                best_action = action
                min_value = action_value
                beta = min(beta, action_value)

                if beta <= alpha:
                    return min_value, best_action

        return min_value, best_action
