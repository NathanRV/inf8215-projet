#!/usr/bin/env python3
"""
Quoridor agent.
Copyright (C) 2013, Nathan Ramsay-Vejlens, Killian Balland

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
from quoridor import *
from minimax import *
from training_board import *

class MyAgent(Agent):

    def __init__(self):
        self.minimax = Minimax(max_branching = 3)
        
        self.minimax.load('minmax')
        self._first_moves : list[Action] = [('P', 1, 4), ('P', 7, 4)]

    """My Quoridor agent."""

    def play(self, percepts, player, step, time_left):
        """
        This function is used to play a move according
        to the percepts, player and time left provided as input.
        It must return an action representing the move the player
        will perform.
        :param percepts: dictionary representing the current board
            in a form that can be fed to `dict_to_board()` in quoridor.py.
        :param player: the player to control in this step (0 or 1)
        :param step: the current step number, starting from 1
        :param time_left: a float giving the number of seconds left from the time
            credit. If the game is not time-limited, time_left is None.
        :return: an action
        eg: ('P', 5, 2) to move your pawn to cell (5,2)
        eg: ('WH', 5, 2) to put a horizontal wall on corridor (5,2)
        for more details, see `Board.get_actions()` in quoridor.py
        """

        if not time_left:
            time_left = math.inf
        
        training_board = dict_to_training_board(percepts)

        if step < 3: # First move is to advance if no wall in front
            if training_board.is_action_valid(self._first_moves[player], player):
                return self._first_moves[player]

        value, action_returned = self.minimax.minimax(training_board, player, time_left)

        return action_returned

if __name__ == "__main__":
    agent_main(MyAgent())

