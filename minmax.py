from training_board import Action, Player, TrainingBoard
import math
import pickle
from heuristics import Heuristics
from typing import Literal
import numpy as np
import random

class Minimax:

    def __init__(self, max_depth: int,
        max_branching: int = 3,
        heuristic: bool = False):
        if max_depth < 1:
            raise ValueError("Max depth must be greater or equals to 1!")

        if max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")

        self.max_depth: int = max_depth
        self.max_branching: int = max_branching
        
        self.h = Heuristics()
        """ 
        self._values : Scores returned by get_score for the player. 
        Key : board, number of moves explored further ahead
        Value : score for player
        """
        self._values : dict[(TrainingBoard, int), int] = dict()
        """ 
        self._best_branching_actions : Action evaluation heuristics. 
        Key : board, player, branching factor
        Value : branching evaluation, branching action, new board after action
        """
        self._best_branching_actions : dict[(TrainingBoard, int, int), [(int, (str, int, int), TrainingBoard)]] = dict()


        self.use_heuristic = heuristic
        #self.h._shortest_path : dict[(TrainingBoard, Literal[0,1]), list] = dict()
        self._best_action : dict[(TrainingBoard, Literal[0,1], int), (int, (str, int, int))] = dict()
        self._acceptable_actions : dict[(TrainingBoard, Literal[0,1], (str, int, int), int, int), bool] = dict()
        self._worthy_actions : dict[(TrainingBoard, Literal[0,1], (str, int, int), int), bool] = dict()
        # TODO :Hyper parameter to define
        # Max lines in front of adversary we consider for walls
        # Max columns beside adversary we consider for walls
        self.max_lines = 2
        self.max_cols = 2
        self.max_evaluation_depth = 1 #Is defined in play function called in heuristic_player.py

    def save(self, filepath: str):
        try:
            if self.use_heuristic:
                with open(filepath+'_heuristic_values.pickle', "wb") as file:
                    pickle.dump(self._values, file, pickle.HIGHEST_PROTOCOL)
            else : 
                with open(filepath+'_scores.pickle', "wb") as file:
                    pickle.dump(self._values, file, pickle.HIGHEST_PROTOCOL)
        except Exception:
            print('Error saving _values!')

        try:
            with open(filepath+'_shortest_path.pickle', "wb") as file:
                pickle.dump(self.h._shortest_path, file, pickle.HIGHEST_PROTOCOL) 
        except Exception:
            print('Error saving _shortest_path!')

        try:
            with open(filepath+'_branching_actions.pickle', "wb") as file:
                pickle.dump(self._best_branching_actions, file, pickle.HIGHEST_PROTOCOL)   
        except Exception:
            print('Error saving _branching_actions!')   


    def load(self, filepath: str):
        try:
            if self.use_heuristic:
                with open(filepath+'_heuristic_values.pickle', "rb") as file:
                    self._values = pickle.load(file)
            else :
                with open(filepath+'_scores.pickle', "rb") as file:
                    self._values = pickle.load(file)
        except Exception:
            print('Error pickling _values!')

        try:
            with open(filepath+'_shortest_path.pickle', "rb") as file:
                self.h._shortest_path = pickle.load(file)
        except Exception:
            print('Error pickling _shortest_path! File name is :'+filepath+'_shortest_path.pickle')

        try:
            with open(filepath+'_branching_actions.pickle', "rb") as file:
                self._best_branching_actions = pickle.load(file)
        except Exception:
            print('Error pickling _branching_actions!')


    def max_value(self, player: Literal[0, 1], state: TrainingBoard, alpha: float, beta: float, depth: int):
        if state.is_finished():
            try:
                if self.use_heuristic:
                    return self.h.score_approximation(state, player), None
                print('End of game at depth of ', depth)
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        if depth >= self.max_depth:
            try:
                if self.use_heuristic:
                    return self.h.score_approximation(state, player), None
                #print('Max depth of ', depth)
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        other_player = (player + 1) % 2
        max_value, best_action = -math.inf, None
        best_branching_actions = self.get_best_branching_actions(state, player)

        branching = 0
        for (_, action, new_state) in best_branching_actions:
            if best_action is None:
                best_action = action

            #if not self.worthy_action(state, player, action):
            #    continue
            
            if (new_state, self.max_depth - depth) in self._values:
                print('Already checked')
                action_value = self._values[(new_state, self.max_depth - depth)]
            else :
                print('Not already checked')
                action_value, _ = self.min_value(player, new_state, alpha, beta, depth+1)
                self._values[(new_state, self.max_depth - depth)] = action_value

            if action_value > max_value :
                best_action = action
                max_value = action_value
                alpha = max(alpha, action_value)

            if action_value >= beta:
                #print('Pruning max')
                return max_value, best_action
            
            branching += 1
            if branching >= self.max_branching:
                break

        return max_value, best_action

    def min_value(self, player: Literal[0, 1], state: TrainingBoard, alpha: float, beta: float, depth: int):
        if state.is_finished():
            try:
                if self.use_heuristic:
                    return self.h.score_approximation(state, player), None
                return self.get_score(state, player), None 
            except Exception:
                return 0, None

        if depth >= self.max_depth:
            try:
                if self.use_heuristic:
                    return self.h.score_approximation(state, player), None
                return self.get_score(state, player), None 
            except Exception:
                return 0, None
            
        min_value, best_action = math.inf, None
        other_player = (player + 1) % 2
        best_branching_actions = self.get_best_branching_actions(state, other_player)

        branching = 0
        for (_, action, new_state) in best_branching_actions:
            if best_action is None:
                best_action = action

            #if not self.worthy_action(state, other_player, action):
            #    continue
   
            if (new_state, self.max_depth - depth) in self._values:
                print('Already checked')
                action_value = self._values[(new_state, self.max_depth - depth)]
            else :
                print('Not already checked')
                action_value, _ = self.max_value(player, new_state, alpha, beta, depth+1)
                self._values[(new_state, self.max_depth - depth)] = action_value

            if action_value < min_value:
                best_action = action
                min_value = action_value
                beta = min(beta, action_value)

            if action_value <= alpha:
                #print('Pruning min')
                return min_value, best_action

            branching += 1
            if branching >= self.max_branching:
                break

        return min_value, best_action
    

    def get_best_branching_actions(
        self, board: TrainingBoard, player: Player, max_branching: int = None
    ) -> list[tuple[float, Action, TrainingBoard]]:

        if max_branching is None:
            max_branching = self.max_branching
        elif max_branching < 1:
            raise ValueError("Max branching must be greater or equals to 1!")

        if (board, player) in self._best_branching_actions : 
            return self._best_branching_actions[(board, player)]

        branching_actions = []
        branching_evaluations = []
        best_branching_actions = []
        test = []

        for branching_action in board.get_actions(player, legal_only=False):
            try:
                action_evaluation = self.h.action_evaluation(
                    board, player, branching_action
                )
                branching_actions.append(branching_action)
                branching_evaluations.append(action_evaluation)

                new_board = board.clone().play_action(branching_action, player)
                test.append((action_evaluation, branching_action, new_board))

            except Exception:
                pass

        #sorted_test = np.sort(test)
        test.sort(key=lambda element: (-element[0], element[1][0]))

        # TODO : change magic number 5
        for i in range(5):
            if i >= len(test):
                break
            best_branching_actions.append(test[i])
        #best_branching_actions = test[0:5]

        
        """
        nb_added_actions = 0
        i = len(branching_evaluations) - 1
        sorted_eval_indexes = np.argsort(branching_evaluations)

        # TODO : change magic number 5
        while i >= 0 and nb_added_actions < 5:
            branching_action = branching_actions[sorted_eval_indexes[i]]
            branching_evaluation = branching_evaluations[sorted_eval_indexes[i]]

            try:
                new_board = board.clone().play_action(branching_action, player)

                #if (new_board, player) not in self._visited_states:
                best_branching_actions.append(
                    (branching_evaluation, branching_action, new_board)
                )
                nb_added_actions += 1

            except Exception:
                pass

            i -= 1"""

        #for i in range(5):
        #    if best_branching_actions[i][0:2] != test[i][0:2]:
        #        print('Not same test action branched')
        #        print('Test action is ', test[i][0:2])
        #        print('Branching action is ', best_branching_actions[i][0:2])
            #if best_branching_actions[i][0:2] != sorted_test[i][0:2]:
            #    print('Not same sorted action branched')
            #    print('Sorted test action is ', sorted_test[i][0:2])
            #    print('Branching action is ', best_branching_actions[i][0:2])

        self._best_branching_actions[(board, player)] = best_branching_actions

        return self._best_branching_actions[(board, player)]

    def get_score(self, board: TrainingBoard, player):
        """Return a score for this board for the given player.

        The score is the difference between the lengths of the shortest path
        of the player minus the one of its opponent. It also takes into
        account the remaining number of walls.
        """
        if (board, player) not in self.h._shortest_path:
            try:  
                self.h._shortest_path[(board, player)] = board.get_solution_path(player)
                #self.h._shortest_path[(board, player)] = self.h.get_path_n_line_closer_goal(board, player)
            except NoPath:           
                self.h._shortest_path[(board, player)] = []
        
        other_player = 1 - player
        if (board, other_player) not in self.h._shortest_path:
            try:
                self.h._shortest_path[(board, other_player)] = board.get_solution_path(other_player)
                #self.h._shortest_path[(board, other_player)] = self.h.get_path_n_line_closer_goal(board, other_player)
            except NoPath:           
                self.h._shortest_path[(board, other_player)] = []

        score = len(self.h._shortest_path[(board, other_player)]) - \
                len(self.h._shortest_path[(board, player)])
        if not score:
            score = board.nb_walls[player] - board.nb_walls[other_player]
        #print('Score is ', score)
        return score

    def random_branch(self, board: TrainingBoard, player: Literal[0, 1]):
        actions = self.get_best_branching_actions(board, player, 5)
        return random.choice(actions)[1]

# TODO : verifier que ces fonctions sont encore utiles et effacer sinon

    def worthy_action(self, board: TrainingBoard, player: Literal[0, 1], action: (str, int, int)):
        if (str(board), player) not in self.h._shortest_path:
            try:
                self.h._shortest_path[(str(board), player)] = board.get_solution_path(player)
                #print('Shortest path found : ', self.h._shortest_path[(str(board), player)])
            except NoPath:           
                self.h._shortest_path[(str(board), player)] = []
        
        if (str(board), player, self.max_evaluation_depth) not in self._best_action:
            self._best_action[(str(board), player, self.max_evaluation_depth)] = (self.h.action_evaluation(board, player, action, self.max_evaluation_depth), action)
            #print('Default best action ', self._best_action[(str(board), player, self.max_evaluation_depth)])

        if not self.acceptable_action(board, player, action): 
            return False

        if (str(board), player, action, self.max_evaluation_depth) in self._worthy_actions:
            return self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)]
        else :
            self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)] = False
        
        # Compare the actions with the best one found yet
        action_evaluation = self.h.action_evaluation(board, player, action, self.max_evaluation_depth)

        # Check if best action is valid first (sometimes not the case if default)
        if not self.acceptable_action(board, player, self._best_action[(str(board), player, self.max_evaluation_depth)][1]):
            #print('Best action by default ', self._best_action[(str(board), player, self.max_evaluation_depth)], 'is not valid' )
            self._best_action[(str(board), player, self.max_evaluation_depth)] = (action_evaluation, action)
            self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)] = True
    
        if action_evaluation == self._best_action[(str(board), player, self.max_evaluation_depth)][0]:
            self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)] = True
        elif action_evaluation > self._best_action[(str(board), player, self.max_evaluation_depth)][0]:
            self._best_action[(str(board), player, self.max_evaluation_depth)] = (action_evaluation, action)
            self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)] = True
        if action[0] == 'P' : #If a move and passed acceptability test, keep it
            self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)] = True
        return self._worthy_actions[(str(board), player, action, self.max_evaluation_depth)]
    
    def acceptable_action(self, board: TrainingBoard, player: Literal[0,1], action: (str, int, int)):
        if (str(board), player, action, self.max_lines, self.max_cols) in self._acceptable_actions:  
            return self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)]    
        else :
            self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)] = True

        # First, we remove any actions that are useless
        if action[0] != 'P':
            # player position
            play_y, play_x = board.pawns[player]
            # opponent position and goal
            oppo_y, oppo_x = board.pawns[1-player]
            oppo_goal_y = board.goals[1-player]
            # Wall values
            wall_dir, wall_y, wall_x = action
            
            if wall_y > play_y + self.max_cols - 1 or wall_y < play_y - self.max_cols : # not near player
                if wall_y > oppo_y + self.max_cols - 1 or wall_y < oppo_y - self.max_cols : # not near adversary
                    self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)] = False
            if wall_x > play_x + self.max_lines - 1 or wall_x < play_x - self.max_lines: # not near player
                if wall_x > oppo_x + self.max_lines - 1 or wall_x < oppo_x - self.max_lines: # not near adversary
                    self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)] = False
        else : # Move is not the first on shortest path
            if (action[1], action[2]) != self.h._shortest_path[(str(board), player)][0] and len(self.h._shortest_path[(str(board), player)]) != 0:
                self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)] = False
        return self._acceptable_actions[(str(board), player, action, self.max_lines, self.max_cols)]
