from training_board import TrainingBoard
import pickle
from typing import Literal
import os
import shutil
from agent_1989944_2159054 import *

shortest_path : LRUCache = LRUCache(500000)
shortest_path_combined : LRUCache = LRUCache(500000)

branching_actions : dict[(TrainingBoard, int, int), [(int, (str, int, int), TrainingBoard)]] = dict()
branching_actions_combined : dict[(TrainingBoard, int, int), [(int, (str, int, int), TrainingBoard)]] = dict()

scores : dict[(TrainingBoard, int), int] = dict()
scores_combined : dict[(TrainingBoard, int), int] = dict()

def update_pickles(number_files: int):
    for i in range(1,number_files+1):
        print('Combining number '+ str(i))
        try:
            with open('minmax'+str(i)+'_shortest_path.pickle', "rb") as file:
                shortest_path = pickle.load(file)
            for k in shortest_path.cache.keys():
                shortest_path_combined.put(k, shortest_path.cache[k])
        except Exception as e:
            print(e)
            print('Error pickling/updating path!', i)
            pass

        try:
            with open('minmax'+str(i)+'_branching_actions.pickle', "rb") as file:
                branching_actions = pickle.load(file)
            branching_actions_combined.update((k, branching_actions[k]) for k in branching_actions.keys())
        except Exception:
            print('Error pickling/updating actions!', i)
            pass

        try:
            with open('minmax'+str(i)+'_scores.pickle', "rb") as file:
                scores = pickle.load(file)
            scores_combined.update((k, scores[k]) for k in scores.keys())
        except Exception:
            print('Error pickling/updating scores!', i)
            pass


    try:
        with open('minmax_shortest_path.pickle', "wb") as file:
            pickle.dump(shortest_path_combined, file, pickle.HIGHEST_PROTOCOL)
    except Exception:
        print('Error saving !')
        pass

    try:
        with open('minmax_branching_actions.pickle', "wb") as file:
            pickle.dump(branching_actions_combined, file, pickle.HIGHEST_PROTOCOL)
    except Exception:
        print('Error saving !')
        pass

    try:
        with open('minmax_scores.pickle', "wb") as file:
            pickle.dump(scores_combined, file, pickle.HIGHEST_PROTOCOL)
    except Exception:
        print('Error saving !')
        pass


    for i in range(1,number_files+1):
        print('Removing number : '+str(i))
        try:
            os.remove('minmax'+str(i)+'_shortest_path.pickle')
        except Exception:
            print('Error removing path!', i)
            pass

        try:
            os.remove('minmax'+str(i)+'_branching_actions.pickle')
        except Exception:
            print('Error removing action!', i)
            pass

        try:
            os.remove('minmax'+str(i)+'_scores.pickle')
        except Exception:
            print('Error removing score!', i)
            pass
        
def copy_pickles(number_files: int):
    for i in range(1,number_files+1):
        src_path = r".\minmax_shortest_path.pickle"
        dst_path = r".\minmax"+str(i)+"_shortest_path.pickle"
        shutil.copy(src_path, dst_path)
        
    for i in range(1,number_files+1):
        src_path = r".\minmax_branching_actions.pickle"
        dst_path = r".\minmax"+str(i)+"_branching_actions.pickle"
        shutil.copy(src_path, dst_path)
        
    for i in range(1,number_files+1):
        src_path = r".\minmax_scores.pickle"
        dst_path = r".\minmax"+str(i)+"_scores.pickle"
        shutil.copy(src_path, dst_path)