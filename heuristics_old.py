""" 
Heuristic functions to evaluate states. 
Some are inspired by the ones mentioned in "A Quoridor-playing Agent" by P.J.C. Mertens on June 21, 2006.
"""

from typing import Literal
from quoridor import Board
Player = Literal[0,1]
Action = tuple[str, int, int]



class Heuristics():
    def __init__(self):
        """
        """
        self._known_values: dict[tuple[BoardRepresentation, Player], int] = dict()
        self.pos_feat_factor = 1
        self.pos_diff_factor = 1
        self.n_line_player_factor = 1
        self.n_line_adversary_factor = 1
        
    """SCORE EVALUATION SECTION, WE ARE LOOKING TO MAXIMIZE THESE VALUES.
    USED IN MINMAX FUNCTIONS"""
   
    def n_line_closer_difference(self, board: Board, player: Player, n = 1):
        """
        We want to maximise this difference.
        Returns the difference in moves to do before getting n line closer between adversary and player.
        Returns 0 if adversary has less moves to do than player.
        """
        difference = self.n_line_closer_goal(board, (player + 1) % 2, n) - \
            self.n_line_closer_goal(board, player, n)
        if difference > 0:
            return difference
        return 0
     
    # Feature 1 : position feature, straight distance between start line and player
    def position_feature(self, board: Board, player: Player):
        """
        Returns the distance in straight line between player's pawn and their adversary's goal.
        """
        return abs(board.pawns[player][0] - board.goals[(player + 1) % 2])

    #Feature 2 from paper
    def position_difference(self, board: Board, player: Player):
        """
        Returns the difference in position feature values between player and adversary.
        Returns 0 if adversary has the advantage.
        """
        difference = self.position_feature(board, player) - \
            self.position_feature(board, (player + 1) % 2)
        if difference > 0:
            return difference
        return 0

    def variation_position_difference(self, board: Board, player: Player, action: Action):
        """
        We want to maximize position difference.
        Returns 1 if the action increases the player's advantage evaluated by the position_difference heuristic.
        Returns 0 if the action decreases or doesn't change the values.
        """
        initial_value = self.position_difference(board, player)
        board = board.clone().play_action(action, player)
        new_value = self.position_difference(board, player)
        if initial_value - new_value < 0 :
            return 1
        return 0


    """HEURISTICS SECTION, WE ARE LOOKING TO MINIMIZE THESE VALUES. 
    CAN BE USED IN MINMAX FUNCTIONS IF USING A FACTOR AND DIVIDING BY VALUE RETURNED"""

    def get_path_n_line_closer_goal(self, board: Board, player: Player, n = 1):
        """ Slightly modified function from quoridor.py get_shortest_path()
        Returns a shortest path for player to reach n lines closer to their goal
        if player is on its goal, the shortest path is an empty list
        if no path exists, exception is thrown.
        """

        def get_pawn_moves(pos):
            (x, y) = pos
            positions = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1), (x - 1, y + 1),
                (x + 2, y), (x - 2, y), (x, y + 2), (x, y - 2)]
            moves = []
            for new_pos in positions:
                if board.is_pawn_move_ok(pos, new_pos,
                    board.pawns[(player + 1) % 2]):
                    moves.append(new_pos)
            return moves

        visited = [[False for i in range(board.size)] for i in range(board.size)]
        # Predecessor matrix in the BFS
        prede = [[None for i in range(board.size)] for i in range(board.size)]

        neighbors = [board.pawns[player]]
        current_x = neighbors[0][0]
        goal = board.goals[player]

        difference = goal - current_x
        if difference >= 0 : # going down
            if difference < n: # less than n lines to go
                next_x = goal
            else :
                next_x = current_x + n
        else : # going up
            if abs(difference) < n: # less than n lines to go
                next_x = goal
            else:
                next_x = current_x - n

        while len(neighbors) > 0:
            neighbor = neighbors.pop(0)
            (x, y) = neighbor
            visited[x][y] = True
            if x == next_x:
                succ = [neighbor]
                curr = prede[x][y]
                while curr is not None and curr != board.pawns[player]:
                    succ.append(curr)
                    (x_, y_) = curr
                    curr = prede[x_][y_]
                succ.reverse()
                return succ
            unvisited_succ = [(x_, y_) for (x_, y_) in
                get_pawn_moves(neighbor) if not visited[x_][y_]]
            for n_ in unvisited_succ:
                (x_, y_) = n_
                if not n_ in neighbors:
                    neighbors.append(n_)
                    prede[x_][y_] = neighbor
        return []

    # Feature 3 and feature 4 from Mertens's paper depending on player chosen if n =1
    def n_line_closer_goal(self, board: Board, player: Player, n = 1):
        """
        Returns number of moves to reach n lines closer to their goal
        if player is on its goal, returns 0
        """
        if board.pawns[player][0] == board.goals[player]:
            return 0
        return len(self.get_path_n_line_closer_goal(board, player, n))

    def distance_goal_feature(self, board: Board, player: Player):
        """
        Returns the distance in straight line between player's pawn and their adversary's goal.
        """
        return abs(board.pawns[player][0] - board.goals[player])

    """Functions that return 1 or 0"""
    def variation_position_feature(self, board: Board, player: Player, action: Action):
        """
        We want to minimize position feature value
        Returns 1 if the action decreases the value evaluated by the position_feature heuristic.
        Returns 0 if the action increases or doesn't change the values.
        """
        initial_value = self.position_feature(board, player)
        board = board.clone().play_action(action, player)
        new_value = self.position_feature(board, player)
        if initial_value - new_value > 0 :
            return 1
        return 0

    # On player, can return -x,...,-1,0,1 
    def variation_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to minimize number of moves before n line closer if our player.
        Returns variation in number of moves (can be negative if increases number of moves).

        #Returns difference in moves if the action reduces the number of moves to get n lines closer to the goal.
        #Returns 0 if the action increases or doesn't change the number of moves needed.
        """
        difference_player = 0
        initial_path = self.get_path_n_line_closer_goal(board, player, n)

        new_board = board.clone().play_action(action, player)

        if (action[1], action[2]) == initial_path[0]: #if move on shortest path
            difference_player = 1
        elif action[0] != 'P': #if wall, check if blocks player
            new_value = self.n_line_closer_goal(new_board, player, n)
            difference_player = len(initial_path) - new_value #-x

        return difference_player

    # On adversary, wall can increase more than one 0,1,...,x
    def block_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to maximize number of moves before n line closer for our adversary.
        Returns variation in number of moves (always positive).
        """
        initial_value = self.n_line_closer_goal(board, 1-player, n)
        new_board = board.clone().play_action(action, player)
        new_value = self.n_line_closer_goal(new_board, 1-player, n)
        return new_value - initial_value

    def block_comparison_n_line_closer_goal(self, board: Board, player: Player, action: Action, n = 1):
        """
        We want to maximize number of moves before n line closer for our adversary and
        minimize number of moves before n line closer for us.
        
        Returns sum of variation in moves (+ for adversary, - for player)
        """
        difference_player = self.variation_n_line_closer_goal(board, player, action, n)
        difference_adv = self.block_n_line_closer_goal(board, player, action, n)

        return difference_adv + difference_player

    def variation_heuristic_choice(self, board: Board, player: Player, action: Action):
        """
        Returns highest value and name of heuristic between variation heuristics :
        Variation of : position feature, position difference or n line closer goal for player and adversary.
        """
        var_pos_feat = self.heuristic_cap(self.variation_position_feature(board, player, action))
        #var_pos_diff = self.heuristic_cap(self.variation_position_difference(board, player, action))
        var_n_line_player = self.heuristic_cap(self.variation_n_line_closer_goal(board, player, action))
        var_n_line_adversary = self.heuristic_cap(-self.variation_n_line_closer_goal(board, (player + 1) % 2, action))
        max_value = max(var_pos_feat, var_pos_diff, var_n_line_player, var_n_line_adversary)
        if max_value == var_pos_feat:
            return max_value, 'Variation position feature'
        #elif max_value == var_pos_diff:
        #    return max_value, 'Variation position difference'
        elif max_value == var_n_line_player:
            return max_value, 'Variation n line closer player'
        return max_value, 'Variation n line closer adversary'

    def general_heuristic_choice(self, board: Board, player: Player):
        """
        TODO : define factors for each heuristic!
        Returns highest value and name of heuristic between general heuristics :
        General heuristics : position feature, position difference or n line closer goal.
        """
        pos_feat = self.heuristic_cap(self.pos_feat_factor * self.position_feature(board, player))
        pos_diff = self.heuristic_cap(self.pos_diff_factor * self.position_difference(board, player))
        n_line_player = self.heuristic_cap(self.n_line_player_factor * self.n_line_closer_goal(board, player))
        n_line_adversary = self.heuristic_cap(self.n_line_adversary_factor * self.n_line_closer_goal(board, (player + 1) % 2))

        max_value = max(pos_feat, pos_diff, n_line_player, n_line_adversary)
        if max_value == pos_feat:
            return max_value, 'Position feature'
        elif max_value == pos_diff:
            return max_value, 'Position difference'
        elif max_value == n_line_adversary : 
            return max_value, 'Adversary n line closer'
        return max_value, 'Player n line closer'

    def heuristic_cap(self, heuristic_value: int):
        """
        Returns shortest distance to goal if heuristics value is higher so that they stay admissible.
        """
        if heuristic_value > self.max_heuristic:
            return self.max_heuristic
        return heuristic_value

        
    def action_evaluation(self, board: Board, player: Player, action: Action):
        """
        Englobing function that returns evaluation value of action.
        """
        print('Evaluating action : ', action)
        evaluation = self.block_comparison_n_line_closer_goal(board, player, action, 3)
        print('Testing feature value ', evaluation)

        return evaluation


    def score_approximation(self, board: Board, player: Player):
        pos_feat = self.position_feature(board, player)
        pos_diff = self.position_difference(board, player)
        moves_next_column = self.n_line_closer_goal(board, player, 1)
        print('Position feature player :', pos_feat)
        print('Position feature difference :', pos_diff)
        print('Moves to next column :', moves_next_column)
        total = pos_feat + pos_diff - moves_next_column
        print('Total is ', total)

        return total



    """TESTING ZONE, DO NOT USE"""

    def test_my_heuristic(self, board: Board, player: Player, action: Action = ('',0,0)):
        """
        Function that evaluates the state according to multiple heuristics and 
        return the highest value which is admissible.
        If no action is passed, no variation heuristic is evaluated.
        """
        self.max_heuristic = len(board.get_shortest_path(player))
        print('Max heuristic value is ', self.max_heuristic)
        if (str(board), player) not in self._known_values:
            var_heuristic = 0, 'None'
            if action[0] != '':
                var_heuristic = self.variation_heuristic_choice(board, player, action)
            gen_heuristic = self.general_heuristic_choice(board, player)
            print('General heuristic chosen is ', gen_heuristic[1], 'with value :', gen_heuristic[0])
            max_feat = max(var_heuristic[0], gen_heuristic[0])
            self._known_values[(str(board), player)] = max_feat
        return self._known_values[(str(board), player)]

    def my_heuristic(self, board: Board, player: Player):
        test = self.score_approximation(board, player)

        return test
        if (str(board), player) not in self._known_values:
            feat1 = self.position_feature(board, player)
            feat2 = self.position_difference(board, player)
            feat3 = 1/self.n_line_closer_goal(board, player, n = 1) # 1 on value since we want to minimize number of moves
            feat4 = self.n_line_closer_goal(board, (player + 1) % 2, n = 2)
            moves_to_goal = len(board.get_shortest_path(player))
            if moves_to_goal != 0:
                feat5 = 1/moves_to_goal
            else :
                feat5 = 10
            total = feat1+feat2+feat3+feat4+feat5
            max_feat = max(feat1, feat2, feat3)
            #print ("Heuristics total is ", total)
            #if total < 0 :
                #print('Current player pos : ', board.pawns[player], ' Adversary pos : ', board.pawns[(player+1)%2] )
            print("Position feature ", feat1)
            print("Difference between position feature ", feat2)
            print("1/number of moves to get n line closer goal ", feat3)
            print("Number of moves to get n line closer goal for enemy ", feat4)
            print("1/number of moves to get to goal ", feat5)
                #return 0
            self._known_values[(str(board), player)] = total
        return self._known_values[(str(board), player)]
    
    def path_heuristic(self, board: Board, player: Player):
        factor = 1.1 #Priorize advancement (attack) rather than defense
        distance_to_goal = len(board.get_shortest_path(player))
        adversary_distance_to_goal = len(board.get_shortest_path((player + 1) % 2))
        print('Current player pos : ', board.pawns[player], ' Adversary pos : ', board.pawns[(player+1)%2] )
        print('Distance to goal', distance_to_goal)
        print('Adversary distance to goal', adversary_distance_to_goal)
        print('Heuristic value',adversary_distance_to_goal - factor * distance_to_goal)

        return adversary_distance_to_goal - factor * distance_to_goal