from typing import Literal
from quoridor import *
import math
import random
import pickle
from heuristics import Heuristics

Action = tuple[str, int, int]
Player = Literal[1]
Probability = float
BoardRepresentation = str

class MCTS():
    def __init__(self, C: float=2.):
        """
        Initialize the Monte Carlo Tree Search.

        Parameters
        ----------
        C : float, optionnal
            Constant used for exploratory policy, the higher it is, the more the tree is explored (default is 2.0).
        """
        self._N: dict[tuple[BoardRepresentation, Player], int] = dict()
        self._U: dict[tuple[BoardRepresentation, Player], int] = dict()
        self._predecessor: dict[tuple[BoardRepresentation, Player], Board] = dict()
        self._successors: dict[tuple[BoardRepresentation, Player], dict[Action, Board]] = {}
        self._P: dict[tuple[BoardRepresentation, Player], dict[Action, Probability]] = {}
        self._visited: set[tuple[BoardRepresentation, Player]] = set()
        self.C = C
        self.heuristics = Heuristics()
    
    def __policy(self, state: Board, player: Player) -> dict[Action, Probability]:
        """
        Returns the policy for a given state of the game.

        Parameters
        ----------
        state : Board
            State of the game.
        player : Player | Literal[1]
            Player for which it is the turn to play.
        
        Returns
        -------
        dict[Action | tuple[str, int, int], Probability | float]
            Dictionnary mapping the possible actions of the player to a probability according to the policy.
        """
        state_representation = (str(state), player)
        if state_representation not in self._P:
            if state_representation not in self._successors:
                actions = state.get_actions(player)
            else:
                actions = list(self._successors[state_representation].keys())

            #self._P[state_representation] = {action: 1 / len(actions) for action in actions}
            print('Actions to add probability :',actions)
            self._P[state_representation] = {action: self.heuristics.my_heuristic(state.clone().play_action(action, player), player) for action in actions}
            #self._P[state_representation] = {action: 1 / ((self.heuristics.heuristic_choice(state, player, action)) or len(actions)) for action in actions}
        
        return self._P[state_representation]
    
    def __action_evaluation(self, state: Board, player: Player, action: Action) -> float:
        """
        Evaluate the return of an action for a given state.

        Parameters
        ----------
        state : Board
            State of the game for which the action is taken.
        player : Player | Literal[1]
            Player performing the action.
        action : Action | tuple[str, int, int]
            Action performed by the player for the given state.
        
        Returns
        -------
        float
            Evaluation of the action taken (range from 0 to +inf).
        """
        state_representation = (str(state), player)
        state_N = self._N[state_representation]
        other_player = PLAYER2 if player == PLAYER1 else PLAYER1
        successor = self._successors[state_representation][action]
        successor_representation = (str(successor), other_player)
        successor_N = self._N[successor_representation]
        if successor_N == 0:
            return math.inf
        successor_U = self._U[successor_representation]
        successor_evaluation = successor_U / successor_N + self.C * self.__policy(state, player)[action] * math.sqrt(math.log(state_N) / successor_N)
        return successor_evaluation
    
    def __select(self, state: Board, player: Player) -> tuple[Board, Player]:
        """
        Select the best leaf state (based on the actions' evaluations) which can be expanded or simulated.

        Parameters
        ----------
        state : Board
            State of the game.
        player : Player
            Player for which it is the turn to play.
        
        Returns
        -------
        tuple[Board, Player | Literal[1]]
            Best leaf state found with its player.
        """
        if state.is_finished():
            return state, player
        
        state_representation = (str(state), player)
        if state_representation not in self._visited:
            self._N[state_representation] = 0
            self._U[state_representation] = 0
            if state_representation not in self._predecessor:
                self._predecessor[state_representation] = None
            self._visited.add(state_representation)
            return state, player
        
        if state_representation not in self._successors:
            return state, player

        best_successor = None
        best_action_evaluation = 0
        other_player = PLAYER2 if player == PLAYER1 else PLAYER1
        for action, successor in self._successors[state_representation].items():
            action_evaluation = self.__action_evaluation(state, player, action)
            if action_evaluation > best_action_evaluation:
                best_successor = successor
                best_action_evaluation = action_evaluation
        
        return self.__select(best_successor, other_player)
    
    def __expand(self, leaf: Board, player: Player) -> tuple[Board, Player]:
        """
        expand a leaf state.

        Parameters
        ----------
        leaf : Board
            Leaf state to expand.
        player : Player | Literal[1]
            Player for which it is the turn to play for the leaf state.
        
        Returns
        -------
        tuple[Board, Player | Literal[1]]
            First child state of the expanded leaf with its player. 
        """
        leaf_representation = (str(leaf), player)
        self._successors[leaf_representation] = {}
        self._P[leaf_representation] = {}

        other_player = PLAYER2 if player == PLAYER1 else PLAYER1
        first_child = None
        actions = leaf.get_actions(player)
        for action in actions:
            child = leaf.clone().play_action(action, player)
            child_representation = (str(child), other_player)
            self._N[child_representation] = 0
            self._U[child_representation] = 0
            self._predecessor[child_representation] = leaf
            self._successors[leaf_representation][action] = child
            #self._P[leaf_representation][action] = 1 / len(actions)
            self._P[leaf_representation][action] = self.heuristics.my_heuristic(child, player)
            if first_child is None:
                first_child = child
        
        return first_child, other_player
    
    def __simulate(self, state: Board, player: Player) -> int:
        """
        Simulate a game using the current policy.

        Parameters
        ----------
        state : Board
            Current state of the game.
        player : Player | Literal[1]
            Player for which it is the turn to play for the leaf state.
        
        Returns
        -------
        int
            Score obtained at the end of the game.
        """
        current_player = player
        actions_done = list()
        depth = 0 # stop simulation after certain number of steps
        while not state.is_finished() and depth < 5: 
            actions = list(self.__policy(state, current_player).keys())
            probabilities = list(self.__policy(state, current_player).values())
            chosen_action = random.choices(actions, weights=probabilities)[0]
            if current_player == player : actions_done.append(chosen_action)
            state = state.clone().play_action(chosen_action, current_player)
            current_player = PLAYER2 if current_player == PLAYER1 else PLAYER1
            depth += 1

        score = state.get_score(player)
        #score = heuristic(player)
        return score, actions_done
    
    def __backpropagate(self, child: Board, child_player: Player, score: int):
        """
        Back propagate a score obtained on a child state.

        Parameters
        ----------
        child : Board
            Child state for which the score has been obtained.
        child_player : Player | Literal[1]
            Player of the child state.
        """
        state = child
        current_player = child_player
        while state is not None:
            state_representation = (str(state), current_player)
            self._N[state_representation] += 1
            self._U[state_representation] += score
            state = self._predecessor[state_representation]
            current_player = PLAYER2 if current_player == PLAYER1 else PLAYER1
    
    def best_action(self, state: Board, player: Player) -> Action:
        """
        Returns the best action for a given state.

        Parameters
        ----------
        state : Board
            State of the game.
        player : Player | Literal[1]
            Player for which it is the turn to play.
        
        Returns
        -------
        Action | tuple[str, int, int]
            Best action found (the most simulated one).
        """
        state_representation = (str(state), player)
        other_player = PLAYER2 if player == PLAYER1 else PLAYER1
        best_action = None
        best_N = 0
        best_U = math.inf
        for action, successor in self._successors[state_representation].items():
            successor_representation = (str(successor), other_player)
            print('Before _N')
            successor_N = self._N[successor_representation]
            print('Before _U')
            successor_U = self._U[successor_representation]
            if successor_N > best_N or (successor_N == best_N and successor_U > best_U):
                best_action = action
                best_N = successor_N
                best_U = successor_U
        
        return best_action

    def explore(self, state: Board, player: Player) -> Action:
        print("Select")
        leaf, leaf_player = self.__select(state, player)
        print("Expand leaf")
        leaf_representation = (str(leaf), leaf_player)
        if self._N[leaf_representation] > 0:
            child, child_player = self.__expand(leaf, leaf_player)
        else:
            child = leaf
            child_player = leaf_player
        print("Simulate child")
        score, actions = self.__simulate(child, child_player)
        print("Back propagate score", score)
        if score > 0:
            print("Actions taken : \n", actions)
        self.__backpropagate(child, child_player, score)
        return score
    
    def save(self, filepath: str):
        with open(filepath+'_P.pickle', "wb") as file:
            pickle.dump(self._P, file, pickle.HIGHEST_PROTOCOL)
        with open(filepath+'_N.pickle', "wb") as file:
            pickle.dump(self._N, file, pickle.HIGHEST_PROTOCOL)
        with open(filepath+'_U.pickle', "wb") as file:
            pickle.dump(self._U, file, pickle.HIGHEST_PROTOCOL)
        with open(filepath+'_pred.pickle', "wb") as file:
            pickle.dump(self._predecessor, file, pickle.HIGHEST_PROTOCOL)
        with open(filepath+'_succ.pickle', "wb") as file:
            pickle.dump(self._successors, file, pickle.HIGHEST_PROTOCOL)
        with open(filepath+'_visited.pickle', "wb") as file:
            pickle.dump(self._visited, file, pickle.HIGHEST_PROTOCOL)
    
    def load(self, filepath: str):
        with open(filepath+'_P.pickle', "r") as file:
            self._P = pickle.load(file)
        with open(filepath+'_N.pickle', "r") as file:
            self._N = pickle.load(file)
        with open(filepath+'_U.pickle', "r") as file:
            self._U = pickle.load(file)
        with open(filepath+'_pred.pickle', "r") as file:
            self._predecessor = pickle.load(file)
        with open(filepath+'_succ.pickle', "r") as file:
            self._successors = pickle.load(file)
        with open(filepath+'_visited.pickle', "r") as file:
            self._visited = pickle.load(file)
