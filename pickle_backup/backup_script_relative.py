import shutil

src_path = r"..\minmax_branching_actions.pickle"
dst_path = r".\minmax_branching_actions.pickle"
shutil.copy(src_path, dst_path)
print('Copied branching actions')

src_path = r"..\minmax_scores.pickle"
dst_path = r".\minmax_scores.pickle"
shutil.copy(src_path, dst_path)
print('Copied scores')

src_path = r"..\minmax_shortest_path.pickle"
dst_path = r".\minmax_shortest_path.pickle"
shutil.copy(src_path, dst_path)
print('Copied shortest path')