
from quoridor import *
import math
import pickle
from heuristics import Heuristics
from typing import Literal

worthy_actions : dict[(str, Literal[0,1], (str, int, int), int), bool] = dict()

converted :  dict[(str, Literal[0,1], (str, int, int), int), bool] = dict()


with open('minmax_worthy_actions.pickle', "rb") as file:
    worthy_actions = pickle.load(file)


for key in worthy_actions.keys():
    if worthy_actions[key] == True:
        converted[key] = True


with open('minmax_worthy_actions.pickle', "wb") as file:
    pickle.dump(converted, file, pickle.HIGHEST_PROTOCOL)

    